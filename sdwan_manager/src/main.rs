#[macro_use]
extern crate log;
extern crate env_logger;

extern crate tokio;
extern crate tokio_util;
extern crate tokio_rustls;
extern crate futures_util;
extern crate futures;
extern crate sdwan_proto;

use std::{
    fs::File,
    sync::Arc,
    net::ToSocketAddrs,
    path::Path,
    io::{self, BufReader},
};
use futures_util::future::TryFutureExt;
use futures::SinkExt;
use tokio::{
    stream::StreamExt,
    runtime,
    net::TcpListener,
};
use tokio_util::codec::{Decoder, Encoder, Framed};
use tokio_rustls::{
    rustls::{
        Certificate,
        NoClientAuth,
        PrivateKey,
        ServerConfig,
        internal::pemfile::{certs, pkcs8_private_keys},
    },
    TlsAcceptor,
};
use sdwan_proto::SDWanCodec;

fn load_certs(path: &Path) -> io::Result<Vec<Certificate>> {
    certs(&mut BufReader::new(File::open(path)?))
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))
}

fn load_keys(path: &Path) -> io::Result<Vec<PrivateKey>> {
    pkcs8_private_keys(&mut BufReader::new(File::open(path)?))
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid key"))
}

fn main() -> io::Result<()> {
    env_logger::init();

    info!("starting sdwan manager");

    let addr = "[::]:8080".to_socket_addrs()?
        .next()
        .ok_or_else(|| io::Error::from(io::ErrorKind::AddrNotAvailable))?;
    let certs = load_certs(Path::new("./rsa/end.fullchain"))?;
    let mut keys = load_keys(Path::new("./rsa/end.key"))?;

    let mut runtime = runtime::Builder::new()
        .threaded_scheduler()
        .enable_io()
        .build()?;
    let handle = runtime.handle().clone();
    let mut config = ServerConfig::new(NoClientAuth::new());
    config.set_single_cert(certs, keys.remove(0))
        .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err))?;
    let acceptor = TlsAcceptor::from(Arc::new(config));

    let fut = async {
        let mut listener = TcpListener::bind(&addr).await?;

        loop {
            let (stream, peer_addr) = listener.accept().await?;
            let acceptor = acceptor.clone();

            let fut = async move {
                let stream = acceptor.accept(stream).await?;
                info!("accepted from {}", peer_addr);

                let mut framed = Framed::new(stream, SDWanCodec::new());
                if let Some(packet) = framed.next().await {
                    let packet = packet?;
                    println!("{:?}", packet);
                    framed.send(packet).await?;
                    framed.flush().await?;
                }

                Ok(()) as io::Result<()>
            };

            handle.spawn(fut.unwrap_or_else(|err| error!("{:?}", err)));
        }
    };

    runtime.block_on(fut)
}
