use tokio_util::codec::{Decoder, Encoder};

use bytes::{Buf, BufMut, BytesMut};
use std::{cmp, fmt, io, str, usize};
use crate::Packet;
use std::error::Error;

#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct SDWanCodec {
    packet_len: Option<u16>,
}

impl SDWanCodec {
    pub fn new() -> SDWanCodec {
        SDWanCodec {
            packet_len: None,
        }
    }
}

impl Decoder for SDWanCodec {
    type Item = Packet;
    type Error = SDWanCodecError;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Packet>, SDWanCodecError> {
        println!("DECODE START {}", buf.len());
        loop {
            match (self.packet_len, buf.len()) {
                (None, r) if r >= 2 => {
                    let len = buf.get_u16();
                    println!("packet len {}", len);
                    buf.reserve(len as usize);
                    self.packet_len = Some(len);
                },
                (Some(len), r) if r >= len as usize  => {
                    let mut data = buf.split_to(len as usize).freeze();
                    self.packet_len = None;
                    return Ok(Some(Packet::decode(&mut data)?))
                },
                _ => return Ok(None)
            }
        }
    }
}

impl Encoder for SDWanCodec {
    type Item = Packet;
    type Error = SDWanCodecError;

    fn encode(&mut self, packet: Packet, buf: &mut BytesMut) -> Result<(), SDWanCodecError> {
        let mut packet_buf = BytesMut::new();
        packet.encode(&mut packet_buf);
        buf.reserve(2 + packet_buf.len());
        buf.put_u16(packet_buf.len() as u16);
        buf.put(packet_buf);
        Ok(())
    }
}

#[derive(Debug)]
pub enum SDWanCodecError {
    Io(io::Error),
}

impl fmt::Display for SDWanCodecError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SDWanCodecError::Io(e) => write!(f, "{}", e),
        }
    }
}

impl From<io::Error> for SDWanCodecError {
    fn from(e: io::Error) -> SDWanCodecError {
        SDWanCodecError::Io(e)
    }
}

impl std::error::Error for SDWanCodecError {}

impl From<SDWanCodecError> for io::Error {
    fn from(e: SDWanCodecError) -> io::Error {
        match e {
            SDWanCodecError::Io(e) => e,
            _ => io::Error::new(io::ErrorKind::Other, e.description())
        }
    }
}
