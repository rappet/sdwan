use bytes::{Buf, BufMut, Bytes, BytesMut};
use std::io;

#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Packet {
    Open,
    Unknown,
}

impl Packet {
    fn type_id(&self) -> u8 {
        match self {
            Open => 0,
            Unknown => 255
        }
    }

    pub fn encode(&self, buf: &mut BytesMut) {
        println!("ENCODE");
        buf.put_u8(self.type_id());
        match self {
            _ => {}
        }
    }

    pub fn decode(buf: &mut Bytes) -> io::Result<(Packet)> {
        println!("DECODE");
        if buf.len() < 1 {
            Err(io::Error::new(io::ErrorKind::InvalidData, "packet type field missing"))?;
        }
        Ok(match buf.get_u8() {
            0 => Packet::Open,
            _ => Packet::Unknown,
        })
    }
}
