extern crate tokio_util;
extern crate bytes;

mod sdwan_codec;
mod packet;

pub use sdwan_codec::SDWanCodec;
pub use packet::Packet;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
