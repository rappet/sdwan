#[macro_use]
extern crate log;
extern crate env_logger;

extern crate tokio;
extern crate tokio_util;
extern crate tokio_rustls;
extern crate futures_util;
extern crate futures;
extern crate sdwan_proto;

use std::{
    io,
    sync::Arc,
};
use futures_util::future::TryFutureExt;
use futures::SinkExt;
use tokio::{
    stream::StreamExt,
    runtime,
    net::TcpStream,
    io::AsyncWriteExt,
};
use tokio_rustls::{TlsConnector, rustls::ClientConfig};
use std::io::BufReader;
use std::fs::File;
use tokio_rustls::webpki::DNSNameRef;
use std::net::ToSocketAddrs;
use sdwan_proto::{Packet, SDWanCodec};
use tokio_util::codec::{Decoder, Encoder, Framed};

fn main() -> io::Result<()> {
    env_logger::init();

    let addr = "127.0.0.1:8080"
        .to_socket_addrs()?
        .next()
        .ok_or_else(|| io::Error::from(io::ErrorKind::NotFound))?;

    let mut runtime = runtime::Builder::new()
        .basic_scheduler()
        .enable_io()
        .build()?;
    let mut config = ClientConfig::new();
    let mut pem = BufReader::new(File::open("rsa/ca.cert")?);
    config.root_store.add_pem_file(&mut pem)
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))?;
    let connector = TlsConnector::from(Arc::new(config));

    let fut = async {
        let stream = TcpStream::connect(&addr).await?;

        let domain = DNSNameRef::try_from_ascii_str("testserver.com")
            .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid dnsname"))?;

        let mut stream = connector.connect(domain, stream).await?;
        let mut framed = Framed::new(stream, SDWanCodec::new());
        let packet1 = Packet::Open;
        println!("Sending packet");
        framed.send(packet1).await?;
        framed.flush().await?;
        info!("Foo");
        if let Some(packet) = framed.next().await {
            let packet = packet?;
            println!("{:?}", packet);
        } else {
            println!(":(")
        }

        Ok(())
    };

    info!("starting sdwan agent");
    runtime.block_on(fut)
}
